const request = require('request');

module.exports.vehicle = async ( req, res, next ) => {
    var options = {
        uri: `http://api.arma-rp.fr/api/vehicles`,
        port: 80,
        method: 'GET',
    }

    request( options, function( error, result, body ){
        var json = JSON.parse( body );
        json = json.data;
        var vh = {};
        var arrayLength = 0;
        for(var k in json){
            
            var tbl = json[k];
            if (tbl.side == "cop"){

                arrayLength = Object.keys(vh).length;

                vh[ arrayLength ] = tbl;
                
            }
        }

        res.render( "info/vehicle", {
            title: "Intranet | Parc Automobile",
            vehicles: vh
        });
    });

    
};