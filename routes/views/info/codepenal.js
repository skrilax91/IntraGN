module.exports.codepenal = async ( req, res, next ) => {

	var type = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM codeType ORDER BY codeType_id', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    var code = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM codeLaw ORDER BY codeLaw_id', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });


    res.render( "info/codepenal", {
        title: "Intranet | Code pénal",
        type: type,
        code: code
    });
};

module.exports.codepenal_add = async ( req, res, next ) => {
    var titre = req.body.title;
    var type = req.body.type;

    if(type == 1){
        var cat = req.body.cat;
        var amende = req.body.amende;
        var sanction = req.body.sanction;
        var opj = req.body.opj;
    }

    if( !app.validPost( titre ) ){
        req.flash('danger', "Titre invalide.");
        res.reload();
        return;
    }

    if( titre.length > 200 || titre.length < 3 ){
        req.flash('danger', "Titre invalide.");
        res.reload();
        return;
    }
    if(type == 0) {
        var exist = await new Promise( ( resolve, reject ) => {
            database.query('SELECT * FROM codeType WHERE codeType_titre = ?', [ titre ], ( err, result ) => {
                if( result.length > 0 ){
                    resolve( true );
                }
            resolve( false );
            })
        });

        if(exist){
            req.flash('warning', "Une catégorie de ce nom existe déjà");
            res.reload();
            return;
        }

        database.query('INSERT INTO codeType( codeType_titre) VALUES(?)', [titre], ( err, result ) => {
            if( err ) throw err;

            req.flash('success', "Catégorie créé avec succès.");
            res.reload();
        })
    }else{
        var exist = await new Promise( ( resolve, reject ) => {
            database.query('SELECT * FROM codeLaw WHERE codeLaw_titre = ? AND codeLaw_category = ?', [ titre, cat ], ( err, result ) => {
                if( err ){ reject(); throw err };
                if( result.length > 0 ){
                    resolve( true );
                }
            resolve( false );
            })
        });

        if(opj){
            opj = true
        }else{
            opj = false
        }

        if(exist){
            req.flash('warning', "Une Loi de ce nom existe déjà");
            res.reload();
            return;
        }

        database.query('INSERT INTO codeLaw( codeLaw_category,codeLaw_titre,codeLaw_amende,codeLaw_sanction,codeLaw_opj) VALUES(?,?,?,?,?)', [cat, titre, amende, sanction, opj], ( err, result ) => {
            if( err ) throw err;

            req.flash('success', "Loi créé avec succès.");
            res.reload();
        })
    }
};

module.exports.codepenal_remove = async ( req, res, next ) => {
    var type = req.body.typedel;

    if(type == 0) {
        var id = req.body.catdel;
        database.query('DELETE FROM codeType WHERE codeType_id = ?', [id], ( err, result ) => {
            if( err ) throw err;

            database.query('DELETE FROM codeLaw WHERE codeLaw_category = ?', [id], ( err, result ) => {
                if( err ) throw err;
    
                req.flash('success', "Catégorie supprimée avec succès.");
                res.reload();
            })
        })

    }else{
        var id = req.body.lawdel;
        database.query('DELETE FROM codeLaw WHERE codeLaw_id = ?', [id], ( err, result ) => {
            if( err ) throw err;

            req.flash('success', "Loi supprimée avec succès.");
            res.reload();
        })
    }
};