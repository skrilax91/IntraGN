function canRead(report, user){

	if(!report.CGR_dest || report.CGR_dest == ""){
		return true;
		
	}

	report = report.CGR_dest.split(",");

	for (var k in report){
		var val = report[k];
		if (val == user){
			return true;
		}
	}

	return false;
}

module.exports.cgr_view = async ( req, res, next ) => {
    id = req.params.id;

    var rapport = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM CGR WHERE CGR_id = ?', [id], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if(rapport.length < 1){
        req.flash('error', "Ce rapport n'éxiste pas !");
        res.redirect('/cgr/')
        return;
    }

    var player = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [rapport.CGR_authid], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    res.render( "cgr/view", {
        title: "Intranet | Lecture du rapport n°"+ rapport.id,
        rapport: rapport,
        player: player
    } );
};