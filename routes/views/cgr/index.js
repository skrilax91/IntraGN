function canRead(report, user){

	if(!report.CGR_dest || report.CGR_dest == ""){
		return true;
		
	}

	report = report.CGR_dest.split(",");

	for (var k in report){
		var val = report[k];
		if (val == user){
			return true;
		}
	}

	return false;
}

module.exports.cgr_index = async ( req, res, next ) => {

    var rapports = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM CGR LEFT JOIN users ON CGR.CGR_authid = users.id ORDER BY `CGR`.`CGR_date` DESC', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    myReports = [];
    reportsList = [];
    trash = [];

    for(var k in rapports){
    	arp = rapports[k];
    	console.log(arp.CGR_date);
    	if (arp.CGR_authid == req.session.auth.id){
    		if (arp.CGR_state == 1){
    			trash.push(arp);
    		}else{
    			myReports.push(arp);
    		}
    	}else if (canRead(arp, req.session.auth.id)){
    		reportsList.push(arp);
    	}
    }

    res.render( "cgr/index", {
        title: "Intranet | C.G.R",
        myReports: myReports,
        reportsList: reportsList,
        trash: trash
    } );
};