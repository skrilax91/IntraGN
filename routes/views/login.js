var randtoken = require('rand-token');
var md5 = require('md5');

module.exports.login = async ( req, res, next ) => {

    if( req.isLogin() ){
        res.redirect( '/' );
        return;
    }

    res.render( "login", {title: "Intranet | Connexion"});
};

module.exports.login_verify = async ( req, res, next ) => {
	var username = req.body.username;
	var pwd = req.body.password;
    var token = randtoken.generate(200);
    var expires = new Date();
	
	if( req.isLogin() || req.isGroup(-1) ){
        res.redirect( '/' );
        return;
    }

    database.query('SELECT * FROM users WHERE name = ? AND password = ?', [ username, md5(pwd) ], ( err, result ) => {
        if( result.length < 1 ){
            req.flash('error', "Nom d'utilisateur ou mot de passe incorrect");
            res.redirect( '/login/' );
        	return;
        }
        req.session.auth = result[0];

        if (req.session.auth.rank == -1) {
            res.clearCookie( "GN_connect_token" );
            req.flash('error', "Vous avez été banni");
            req.session.auth = null;
            req.user = null;
            res.redirect( '/' );
            return
        }

        expires.setTime( expires.getTime() + ( 365 * 24 * 60 * 60 * 1000 ) );
        res.cookie( "GN_connect_token", token, { expires: expires }, );
        database.query('UPDATE users SET login_token = ? WHERE id = ?', [ token, req.session.auth.id ] );

        if(req.session.auth.state == 0){
            res.redirect( '/register/' );
            return
        }
        req.flash('success', "Vous êtes bien connecté");
        res.redirect( '/' );
    })
};

module.exports.register_verify = async ( req, res, next ) => {
    var mdp = req.body.pass;
    var confirm = req.body.confirm;
    
    if( !req.isLogin() || req.isGroup(-1) || req.session.auth.state != 0){
        res.redirect( '/' );
        return;
    }

    if (mdp != confirm) {
        req.flash('error', "Les deux mots de passes ne correspondent pas !");
        res.redirect( '/register/' );
        return;
    }

    if (!(/[a-z]/.test(mdp)) || !(/[A-Z]/.test(mdp)) || mdp.length < 8) {
        req.flash('error', "Votre mot de passe doit contenir au moin 8 caractères et une majuscule");
        res.redirect( '/register/' );
        return;
    }

    database.query('UPDATE users SET password = ?, state = 1 WHERE id = ?', [ md5(mdp), req.session.auth.id ] );
    req.flash('success', "Votre mot de passe à été modifié");
    res.redirect( '/' );
};

module.exports.logout = ( req, res, next ) => {
    res.clearCookie( "GN_connect_token" ); 
    req.session.auth = null;
    req.user = null;
    res.redirect('/');
};