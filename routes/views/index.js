module.exports.index = async ( req, res, next ) => {

    res.render( "index", {
        title: "Intranet | Accueil"
    });
};

module.exports.index_skiptuto = async ( req, res, next ) => {
    req.updatePerm('tuto', 0);
    res.send("");
};