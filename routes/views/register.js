module.exports.register = async ( req, res, next ) => {

    if( !req.isLogin() || req.session.auth.state != 0){
        res.redirect( '/' );
        return;
    }

    res.render( "register", {
        title: "Intranet | Enregistrement"
    } );
};