module.exports.fichiers_interpol = async ( req, res, next ) => {

    var peines = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM peines LEFT JOIN codeLaw ON peines.code_id = codeLaw.codeLaw_id LEFT JOIN casiers ON peines.casier_id = casiers.casiers_id  LEFT JOIN codeType ON codeLaw.codeLaw_category = codeType.codeType_id WHERE peine_presented = 0 ORDER BY peine_amende DESC', ( err, results ) => {
            if( err ){ reject(); throw err };
    
            resolve( results || {} );
        })
    });

    res.render( "fichiers/interpol", {
        title: "Intranet | Interpol",
        peines: peines,
    } );
};