module.exports.fichiers_view = async ( req, res, next ) => {
    var id = req.params.id;

    var casier = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM casiers WHERE casiers_id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    if( casier.length < 1 ){
        res.redirect('/fichiers/')
        return;
    }
    var casier = casier[0]

    var peines = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM peines LEFT JOIN codeLaw ON peines.code_id = codeLaw.codeLaw_id LEFT JOIN codeType ON codeLaw.codeLaw_category = codeType.codeType_id WHERE casier_id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };
    
            resolve( results || {} );
        })
    });

    var type = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM codeType ORDER BY codeType_id', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    var code = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM codeLaw ORDER BY codeLaw_id', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    res.render( "fichiers/view", {
        title: "Intranet | Casier N°" + id,
        casier: casier,
        peines: peines,
        type: type,
        code: code
    } );
};

module.exports.fichiers_config_updateinfo = async ( req, res, next ) => {
    var id = req.params.id;
    var prenom = req.body.prenom;
    var nom = req.body.nom;
    var birthday = req.body.birthday;
    var phone = req.body.phone;
    var rebelle = req.body.rebelle;

    if( !app.validPost( prenom ) ){
        req.flash('danger', "Prénom invalide.");
        console.log("Prénom invalide.");
        res.reload();
        return;
    }

    if( !app.validPost( nom ) ){
        req.flash('danger', "Nom invalide.");
        console.log("Nom invalide.");
        res.reload();
        return;
    }

    if( prenom.length > 50 || nom.length > 50 ){
        req.flash('danger', "Nom ou prénom invalide.");
        console.log("Nom invalide.");
        res.reload();
        return;
    }

    if( !app.validPost( phone ) ){
        phone = null;
    }

    if(rebelle){
        rebelle = true
    }else{
        rebelle = false
    }

    var casier = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM casiers WHERE casiers_id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    if( casier.length < 1 ){
        res.redirect('/fichiers/')
        return;
    }
    var casier = casier[0]

    if( !app.validPost( birthday ) ){
        birthday = casier.casiers_birthday;
    }

    database.query('UPDATE casiers SET casiers_prenom = ?, casiers_nom = ?, casiers_birthday = ?, casiers_telephone = ?, casiers_rebelle = ?  WHERE casiers_id = ?', [ prenom, nom, birthday, phone, rebelle, id ] );
    res.reload();
};

module.exports.fichiers_addpeine = async ( req, res, next ) => {
    var id = req.params.id;
    var code_id = req.body.motif;
    var gn = req.session.auth.name;
    if(typeof code_id == "object"){

        

        for(var k in code_id){
            var peineid = code_id[k];
            var amende = req.body["amende"+peineid];
            var point = req.body["point"+peineid];
            var present = req.body["present"+peineid];
            var comment = req.body["comment-"+peineid];
            

            if(present){
                present = true
            }else{
                present = false
            }
            var code = await new Promise( ( resolve, reject ) => {
                database.query( 'SELECT * FROM codeLaw WHERE codeLaw_id = ?', [ peineid ], ( err, results ) => {
                    if( err ){ reject(); throw err };
        
                    resolve( results[0] || {} );
                })
            });

            database.query('INSERT INTO peines(casier_id, peine_gn, code_id, code_name, peine_amende, peine_point, peine_presented, created_at, peine_comment) VALUES( ?, ?, ?, ?, ?, ?, ?, NOW(), ?)', [ id, gn, peineid, code.codeLaw_titre, amende, point, present, comment], ( err, result ) => {
                if( err ) throw err;
            })
            req.flash('success', "Peines ajoutées avec succès.");
        }
    }else{

        code_id = Number(code_id);

        var amende = req.body["amende"+code_id];
        var point = req.body["point"+code_id];
        var present = req.body["present"+code_id];
        var comment = req.body["comment-"+code_id];

        if(present){
            present = true
        }else{
            present = false
        }

        var code = await new Promise( ( resolve, reject ) => {
            database.query( 'SELECT * FROM codeLaw WHERE codeLaw_id = ?', [ code_id ], ( err, results ) => {
                if( err ){ reject(); throw err };
    
                resolve( results[0] || {} );
            })
        });

        database.query('INSERT INTO peines(casier_id, peine_gn, code_id, code_name, peine_amende, peine_point, peine_presented, created_at, peine_comment) VALUES( ?, ?, ?, ?, ?, ?, ?, NOW(), ?)', [ id, gn, code_id, code.codeLaw_titre, amende, point, present, comment], ( err, result ) => {
            if( err ) throw err;
            req.flash('success', "Peine ajoutée avec succès.");
        })
    }
    res.reload();

};

module.exports.fichiers_updatepeine = async ( req, res, next ) => {
    var id = req.params.id2;
    var code_id = req.body.motif;
    var amende = req.body.amende;
    var point = req.body.point;
    var present = req.body.present;
    var comment = req.body.comment;

    if(present){
        present = true
    }else{
        present = false
    }

    database.query('UPDATE peines SET code_id = ?, peine_amende = ?, peine_point = ?, peine_presented = ?, peine_comment = ? WHERE peine_id = ?', [code_id, amende, point, present, comment, id], ( err, result ) => {
        if( err ) throw err;

        req.flash('success', "Peine modifiée avec succès.");
        res.reload();
    })

};

module.exports.fichiers_deletepeine = async ( req, res, next ) => {
    var id = req.params.id2;

    database.query('DELETE FROM peines WHERE peine_id = ?', [ id ] );
    res.reload();

};