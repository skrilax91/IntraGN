module.exports.fichiers_index = async ( req, res, next ) => {
    var casiers = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM casiers ORDER BY casiers_id DESC', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    res.render( "fichiers/index", {
        title: "Intranet | Liste des fichiers",
        casiers: casiers
    } );
};

module.exports.fichiers_add = async ( req, res, next ) => {
    var prenom = req.body.prenom;
    var nom = req.body.nom;
    var birthday = req.body.birthday;
    var phone = req.body.phone;



    if( !app.validPost( prenom ) ){
        req.flash('danger', "Prénom invalide.");
        res.reload();
        return;
    }

    if( !app.validPost( nom ) ){
        req.flash('danger', "Nom invalide.");
        res.reload();
        return;
    }

    if( !app.validPost( phone ) ){
        phone = "0";
    }

    if(isNaN(Date.parse(birthday)))
    {
        birthday = "0000-00-00 00:00:00"
    }

    if( prenom.length > 50 || nom.length > 50 ){
        req.flash('danger', "Nom invalide.");
        res.reload();
        return;
    }

    var exist = await new Promise( ( resolve, reject ) => {
        database.query('SELECT * FROM casiers WHERE casiers_prenom = ? AND casiers_nom = ?', [ prenom, nom ], ( err, result ) => {
            if( result.length > 0 ){
                resolve( true );
            }
           resolve( false );
        })
    });

    if(exist){
        req.flash('warning', "Un casier existe déjà avec ce nom et prénom");
        res.reload();
        return;
    }

    database.query('INSERT INTO casiers( casiers_prenom, casiers_nom, casiers_register_at, casiers_birthday, casiers_telephone, casiers_photo) VALUES( ?, ?, NOW(), ?, ?, ?)', [ prenom,  nom, birthday, phone, "default" ], ( err, result ) => {
        if( err ) throw err;

        req.flash('success', "Fichier créer avec succès.");
        res.redirect("/fichiers/view/" + result.insertId );
    })
    

};