const request = require('request');

module.exports.siv_result = async ( req, res, next ) => {
    var plaque = req.body.plaque;
    var info = {};

    var options = {
        uri: `http://api.arma-rp.fr/api/vehicles`,
        port: 80,
        method: 'GET',
    }

    

    request( options, function( error, result, body ){
        var json = JSON.parse( body );
        json = json.data;
        for(var k in json){
            
            var tbl = json[k];
            if (tbl.immatriculation == plaque){
                info.vl = tbl;
            }
        }
        var url = "http://api.arma-rp.fr/api/players/";
        if(info.vl == null){
            info.error = 1;
        }else{
            url = "http://api.arma-rp.fr/api/players/"+info.vl.owner_pid;
        }

        var options2 = {
            uri: url,
            port: 80,
            method: 'GET',
        }
        request( options2, function( error, result, body ){
            var json = JSON.parse( body );
            info.prop = json.data;
            res.render( "siv/result", {
                title: "Intranet | S.I.V",
                result: info
            } );
        });
    });
    
};