var config = require('../../../config.json');
var md5 = require('md5');
module.exports.users_index = async ( req, res, next ) => {
    var users = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users ORDER BY grade DESC', ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    res.render( "users/index", {
        title: "Intranet | Liste des effectifs",
        users: users
    } );
};

module.exports.users_add = async ( req, res, next ) => {
    var name = req.body.name;
    var join = req.body.joinat;
    var grade = req.body.grade;
    var phone = req.body.phone;

    if( !app.validPost( name ) ){
        req.flash('danger', "Nom invalide.");
        console.log("nom invalide");
        res.reload();
        return;
    }

    if( !app.validPost( phone ) ){
        phone = "0";
    }

    if(name.length > 50 ){
        req.flash('danger', "Nom invalide.");
        console.log("nom invalide 50");
        res.reload();
        return;
    }


    database.query('INSERT INTO users(name, password, grade, join_at, settings) VALUES(?, ?, ?, ?, ?)', [ name, md5(name), grade, join , JSON.stringify(config.defaultSettings)], ( err, result ) => {
        if( err ) throw err;
        console.log(JSON.stringify(config.defaultSettings));

        req.flash('success', "Effectif créé créer avec succès.");
        console.log("effectif créé");
        res.reload();
    })

};