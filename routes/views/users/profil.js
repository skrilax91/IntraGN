var config = require('../../../config.json');
module.exports.users_profil = async ( req, res, next ) => {
    var id = req.params.id;

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });



    if( profil.length < 1 ){
        res.redirect('/users/')
        return;
    }

    var pds = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM PDS WHERE PDS_userid = ? ORDER BY PDS_id DESC', [profil.id] , ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    res.render( "users/profil", {
        title: "Intranet | Profil",
        profil: profil,
        pds: pds
    } );
};

module.exports.users_changeperms = async ( req, res, next ) => {
    var id = req.params.id;
    //var  = req.body. ;

    if(!JSON.parse(req.session.auth.settings).perms.manageUser.changeperms){
        req.flash('error', "Vous n'avez pas la permission !");
        res.redirect('/users/profil/'+id)
        return;
    }

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if( profil.length < 1 ){
        req.flash('error', "Cet utilisateur n'éxiste pas !");
        res.redirect('/users/')
        return;
    }


    var settings = config.defaultSettings;
    ////////////////////////////////////////
    //        MODIFICATION DES VAR        //
    ////////////////////////////////////////
    settings.EM = req.body.EM;
    settings.tuto = req.body.tuto;
    if(settings.perms){
        if (settings.perms.manageUser) {
            settings.perms.manageUser.changeName = req.body.changeName;
            settings.perms.manageUser.changePass = req.body.changePass;
            settings.perms.manageUser.changeJoinAt = req.body.changeJoinAt;
            settings.perms.manageUser.changeAvatar = req.body.changeAvatar;
            settings.perms.manageUser.promote = req.body.promote;
            settings.perms.manageUser.demote = req.body.demote;
            settings.perms.manageUser.suspend = req.body.suspend;
            settings.perms.manageUser.changeperms = req.body.changeperms;
            settings.perms.manageUser.deleteUser = req.body.deleteUser;
            settings.perms.manageUser.addUser = req.body.addUser;
        }
    }

    
    console.log(settings);
    settings = JSON.stringify(settings);

    database.query('UPDATE users SET settings = ? WHERE id = ?', [ settings, id ] );
    req.flash('success', "Permissions modifiées");
    res.reload();
};

module.exports.users_promote = async ( req, res, next ) => {
    var id = req.params.id;

    if(!JSON.parse(req.session.auth.settings).perms.manageUser.promote){
        req.flash('error', "Vous n'avez pas la permission !");
        res.redirect('/users/profil/'+id)
        return;
    }

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if( profil.length < 1 ){
        req.flash('error', "Cet utilisateur n'éxiste pas !");
        res.redirect('/users/')
        return;
    }

    if( config.ranks[profil.grade + 1] == null ){
        req.flash('error', "Cet utilisateur ne peut pas etre promus !");
        res.redirect('/users/profil/'+id)
        return;
    }

    database.query('UPDATE users SET grade = ? WHERE id = ?', [ profil.grade + 1, id ] );
    req.flash('success', "Utilisateur promus");
    res.reload();
};

module.exports.users_demote = async ( req, res, next ) => {
    var id = req.params.id;

    if(!JSON.parse(req.session.auth.settings).perms.manageUser.demote){
        req.flash('error', "Vous n'avez pas la permission !");
        res.redirect('/users/profil/'+id)
        return;
    }

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if( profil.length < 1 ){
        req.flash('error', "Cet utilisateur n'éxiste pas !");
        res.redirect('/users/')
        return;
    }

    if( profil.grade <= 1 ){
        req.flash('error', "Cet utilisateur ne peut pas etre rétrogradé !");
        res.redirect('/users/profil/'+id)
        return;
    }

    database.query('UPDATE users SET grade = ? WHERE id = ?', [ profil.grade - 1, id ] );
    req.flash('success', "Utilisateur demote");
    res.reload();
};

module.exports.users_admin = async ( req, res, next ) => {
    var id = req.params.id;
    var state = req.params.state;

    if(!req.session.auth.admin){
        req.flash('error', "Vous n'avez pas la permission !");
        res.redirect('/users/profil/'+id)
        return;
    }

    if(state != 0 && state != 1){
        req.flash('error', "Error");
        res.redirect('/users/profil/'+id)
        return;
    }

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if( profil.length < 1 ){
        req.flash('error', "Cet utilisateur n'éxiste pas !");
        res.redirect('/users/')
        return;
    }

    database.query('UPDATE users SET admin = ? WHERE id = ?', [ state, id ] );
    req.flash('success', "Utilisateur Changé");
    res.reload();
};

module.exports.users_suspend = async ( req, res, next ) => {
    var id = req.params.id;
    var state = req.params.state;

    if(!JSON.parse(req.session.auth.settings).perms.manageUser.suspend){
        req.flash('error', "Vous n'avez pas la permission !");
        res.redirect('/users/profil/'+id)
        return;
    }

    if(state != 2 && state != 1){
        req.flash('error', "Error");
        res.redirect('/users/profil/'+id)
        return;
    }

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if( profil.length < 1 ){
        req.flash('error', "Cet utilisateur n'éxiste pas !");
        res.redirect('/users/')
        return;
    }

    database.query('UPDATE users SET state = ? WHERE id = ?', [ state, id ] );
    req.flash('success', "Utilisateur Changé");
    res.reload();
};

module.exports.users_delete = async ( req, res, next ) => {
    var id = req.params.id;

    if(!JSON.parse(req.session.auth.settings).perms.manageUser.deleteUser){
        req.flash('error', "Vous n'avez pas la permission !");
        res.redirect('/users/profil/'+id)
        return;
    }

    var profil = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM users WHERE id = ?', [ id ], ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results[0] || {} );
        })
    });

    if( profil.length < 1 ){
        req.flash('error', "Cet utilisateur n'éxiste pas !");
        res.redirect('/users/')
        return;
    }

    database.query('DELETE FROM users WHERE id = ?', [id] );
    req.flash('success', "Utilisateur supprimé");
    res.redirect('/users/');
};