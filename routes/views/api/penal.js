module.exports.api_get_penal_code = async ( req, res, next ) => {
    var id = req.params.id;

    if (id){
        var code = await new Promise( ( resolve, reject ) => {
            database.query( 'SELECT * FROM codeRoute WHERE code_id = ?', [id], ( err, results ) => {
                if( err ){ reject(); throw err };
                
                if( results.length < 1 ){
                    resolve( {} )
                } else {
                    resolve( results[0] || {} );
                }
            })
        });

        if( code.length < 1 ){
            res.send(`{}`);
            return;
        }

        res.send(code);
        return;
    }else{
        var code = await new Promise( ( resolve, reject ) => {
            database.query( 'SELECT * FROM codeRoute', ( err, results ) => {
                if( err ){ reject(); throw err };
                
                if( results.length < 1 ){
                    resolve( {} )
                } else {
                    resolve( results || {} );
                }
            })
        });
        res.send(code);
        return;
    }

    res.send("lol");
    
    
};
