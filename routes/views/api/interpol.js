module.exports.api_interpol = async ( req, res, next ) => {

    var code = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM peines LEFT JOIN codeRoute ON peines.code_id = codeRoute.code_id LEFT JOIN casiers ON peines.casier_id = casiers.casiers_id WHERE peine_presented = 0 ORDER BY codeRoute.code_type DESC', ( err, results ) => {
            if( err ){ reject(); throw err };
            
            if( results.length < 1 ){
                resolve( {} )
            } else {
                resolve( results || {} );
            }
        })
    });
    res.send(code);
};
