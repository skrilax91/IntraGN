module.exports.absences_index = async ( req, res, next ) => {
    var absences = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM absences WHERE author_id = ? ORDER BY ticket_id DESC', [req.session.auth.id] , ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    res.render( "absences/index", {
        title: "Intranet | Mes absences",
        absences: absences,
    } );
}; 