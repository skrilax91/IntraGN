module.exports.services_index = async ( req, res, next ) => {
    var pds = await new Promise( ( resolve, reject ) => {
        database.query( 'SELECT * FROM PDS WHERE PDS_userid = ? ORDER BY PDS_id DESC', [req.session.auth.id] , ( err, results ) => {
            if( err ){ reject(); throw err };

            resolve( results || {} );
        })
    });

    res.render( "services/index", {
        title: "Intranet | Prise de service",
        services: pds,
    } );
};

module.exports.services_addPDS = async ( req, res, next ) => {
    var id = req.session.auth.id;
    var date = req.body.day;
    var start = "2000-01-01 "+req.body.start+":00";
    var end = "2000-01-01 "+req.body.end+":00";

    if(!date || !start || !end){
        req.flash('error', "Un des champs est érroné");
        res.reload();
        return;
    }

    database.query('INSERT INTO PDS(PDS_userid, PDS_date, PDS_start, PDS_end) VALUES(?, ?, ?, ?)', [ id, date, start, end], ( err, result ) => {
        if( err ) throw err;

        req.flash('success', "Votre prise de service à été prise en compte.");
        res.reload()
    })
};