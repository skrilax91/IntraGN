var express = require('express');
var router = express.Router();

const { catchErrors } = require('../class/errorHandlers.js');
var { maintenance } = require('./views/maintenance');
var { inforadio } = require('./views/info/radio');
var { info } = require('./views/info/info');
var { vehicle } = require('./views/info/vehicle');
var { codepenal, codepenal_add, codepenal_remove } = require('./views/info/codepenal');
var { reglementinstit } = require('./views/info/reglement');
var { ban } = require('./views/ban');
var { index, index_skiptuto } = require('./views/index');
var { register } = require('./views/register');
var { login, login_verify, logout, register_verify } = require('./views/login');
var { fichiers_index, fichiers_add} = require('./views/fichiers/index');
var { users_index, users_add} = require('./views/users/index');
var {
	users_profil,
	users_changeperms,
	users_promote,
	users_demote,
	users_delete,
	users_admin,
	users_suspend
} = require('./views/users/profil');
var {
	fichiers_view,
	fichiers_addpeine,
	fichiers_updatepeine,
	fichiers_deletepeine,
	fichiers_config_updateinfo,
	fichiers_config_updatestate
} = require('./views/fichiers/view');
var { fichiers_interpol } = require('./views/fichiers/interpol');
var { services_index, services_addPDS } = require('./views/services/index');
var { sim_index } = require('./views/sim/index');
var { cgr_index } = require('./views/cgr/index');
var { siv_index } = require('./views/siv/index');
var { siv_result } = require('./views/siv/result');
var { 
	cgr_create,
	cgr_add
} = require('./views/cgr/create');
var { cgr_view } = require('./views/cgr/view');

var { api_get_penal_code } = require('./views/api/penal');
var { api_interpol } = require('./views/api/interpol');

// Homes pages
router.get('/', catchErrors(index));
router.get('/index/skiptuto/', catchErrors(index_skiptuto));
router.get('/maintenance/', catchErrors(maintenance));

// Auth pages
router.get('/login/', catchErrors(login));
router.get('/logout/', catchErrors(logout));
router.get('/register/', catchErrors(register));
router.get('/ban/', catchErrors(ban));
router.post('/login/register/', catchErrors(register_verify));
router.post('/login/verify/', catchErrors(login_verify));

// info pages
router.get('/info/radio', catchErrors(inforadio, 1));
router.get('/info/', catchErrors(info, 1));
router.get('/info/vehicle', catchErrors(vehicle, 1));
router.get('/info/reglement', catchErrors(reglementinstit, 1));
router.get('/info/code', catchErrors(codepenal, 1));
router.post('/info/code/add', catchErrors(codepenal_add, 1));
router.post('/info/code/remove', catchErrors(codepenal_remove, 1));

// users pages
router.get( '/users/', catchErrors( users_index, 1 ));
router.get( '/users/profil/:id', catchErrors( users_profil, 1 ));
router.post( '/users/profil/:id/changeperms', catchErrors( users_changeperms, 1 ));
router.get( '/users/profil/:id/promote', catchErrors( users_promote, 1 ));
router.get( '/users/profil/:id/demote', catchErrors( users_demote, 1 ));
router.get( '/users/profil/:id/delete', catchErrors( users_delete, 1 ));
router.get( '/users/profil/:id/suspend/:state', catchErrors( users_suspend, 1 ));
router.get( '/users/profil/:id/admin/:state', catchErrors( users_admin, 1 ));
router.post( '/users/add/', catchErrors( users_add, 1 ));

// Casiers pages
router.get( '/fichiers/', catchErrors( fichiers_index, 1 ));
router.get( '/fichiers/interpol', catchErrors( fichiers_interpol, 1 ));
router.post( '/fichiers/add/', catchErrors( fichiers_add, 1 ));
router.get( '/fichiers/view/:id', catchErrors( fichiers_view, 1 ));
router.post( '/fichiers/view/:id/addpeine', catchErrors( fichiers_addpeine, 1 ));
router.post( '/fichiers/view/:id/edit/:id2', catchErrors( fichiers_updatepeine, 1 ));
router.get( '/fichiers/view/:id/delete/:id2', catchErrors( fichiers_deletepeine, 1 ));
router.post( '/fichiers/view/:id/config/updateinfo', catchErrors( fichiers_config_updateinfo, 1 ));

// S.I.M pages
router.get( '/sim/', catchErrors( sim_index, 1 ));

// C.G.R pages
router.get( '/cgr/', catchErrors( cgr_index, 1 ));
router.get( '/cgr/create', catchErrors( cgr_create, 1 ));
router.post( '/cgr/add', catchErrors( cgr_add, 1 ));
router.get( '/cgr/view/:id', catchErrors( cgr_view, 1 ));

// C.I.V pages
router.get( '/siv/', catchErrors( siv_index, 1 ));
router.post( '/siv/result', catchErrors( siv_result, 1 ));

// API
router.get( '/api/GetCode/:id', catchErrors( api_get_penal_code, 1 ));
router.get( '/api/GetCode', catchErrors( api_get_penal_code, 1 ));
router.get( '/api/GetInterpol', catchErrors( api_interpol, 1 ));

// Services pages
router.get( '/services/', catchErrors( services_index, 1 ));
router.post( '/services/addPDS/', catchErrors( services_addPDS, 1 ));

module.exports = router;