var config = require('../config.json');
var fs = require('fs');

module.exports = async function( req, res, next ){
    res.locals.user = {}
    
    if( req.session.auth ) {
        if (typeof req.session.auth.permissions === "string" || req.session.auth.permissions instanceof String)
            req.session.auth.permissions = JSON.parse( req.session.auth.permissions )
        res.locals.user.auth = req.session.auth
    }
    
    req.flash = function( type, content ) {
        if( req.session.flash == undefined ){
            req.session.flash = {}
        }
        req.session.flash.ftype = type
        req.session.flash.fcontent = content
    }

    req.isLogin = function() {
        return ( req.session.auth != null );
    }

    req.isBanned = function() {
        return (req.isLogin() && req.session.auth.state == 2 );
    }

    req.isGroup = function( group ) {
        return (req.isLogin() && req.session.auth.grade == group );        
    }

    req.isAdmin = function() {
        return (req.isLogin() && req.session.auth.admin);
    }

    req.checkPerm = function(perm) {
        return (req.isLogin() && (req.session.auth.settings[perm] || req.isAdmin()));
    }

    req.updatePerm = function(perm, val) {
        if (val <= 0 && req.session.auth.permissions)
            delete req.session.auth.permissions[perm];
        else if (val > 0)
            req.session.auth.permissions[perm] = "on";
        else
            return;
        database.query('UPDATE users SET permissions = ? WHERE id = ?', [ JSON.stringify(req.session.auth.permissions), req.session.auth.id ]);
    }

    req.isAccred = function(grade) {
        return (req.isLogin() && (req.session.auth.grade >= grade || req.isAdmin()));
    }

    req.getId = function() {
        if( !req.isLogin() ){ return 0; };
        return req.session.auth.id;
    }

    // Responses
    res.locals.user.isLogin = function() {
        return (res.locals.user.auth != null)
    }

    res.locals.user.isBanned = function() {
        return (req.isLogin() && res.locals.user.auth.state == 2)      
    }

    res.locals.user.isGroup = function(group){
        return (req.isLogin() && res.locals.user.auth.grade == group);
    }

    res.locals.user.isAdmin = function() {
        return (req.isLogin() && res.locals.user.auth.admin);
    }

    res.locals.user.checkPerm = function(perm){
        return (req.isLogin() && (res.locals.user.auth.permissions[perm] || req.isAdmin()));;
    }

     res.locals.user.isAccred = function(grade){
        return (req.isLogin() && (res.locals.user.auth.grade >= grade || req.isAdmin()));
    }

    res.locals.hasFlash = function(){
        if(req.session.flash){ return true; };
        return false;
    }

    res.locals.launchFlash = function(){
        var flash = {};
        flash.type = req.session.flash.ftype
        flash.content = req.session.flash.fcontent
        req.session.flash = undefined

        return flash;
    }

    res.locals.user.getId = function(){
        if( !req.isLogin() ){ return 0; };
        return res.locals.user.auth.id;
    }

    res.reload = function(){
        var url = req.get('referer');
        if( url == undefined ){ url = "/"; };
        res.redirect( url );
    }
    
    next()
}