module.exports = async ( req, res, next ) => {
    if( req.session.auth == null ){ next(); return; }
    database.query('UPDATE `users` SET `last_connection` = NOW() WHERE `id` = ?', [req.session.auth.id] )
    next();
}