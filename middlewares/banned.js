module.exports = async function( req, res, next ){
	if(req.isBanned() && (req.originalUrl != "/ban" && req.originalUrl != "/ban/")){
	    res.redirect('/ban/');
	}else{
		next()
	}
}