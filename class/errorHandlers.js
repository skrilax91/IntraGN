module.exports.catchErrors = (fn, rank) => {
    return function( request, response, next ) {
        if(rank != null && !request.isAccred( rank ) ) { 
            response.redirect('/')
            return;
        }
        return fn( request, response, next ).catch((e) => {
            if ( e.response ) {
                e.status = e.response.status
            }
            
            next( e )
        })
    }
}